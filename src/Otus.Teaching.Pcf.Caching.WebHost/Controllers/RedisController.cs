﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Caching.WebHost.Contracts;
using StackExchange.Redis;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Caching.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RedisController : ControllerBase
    {
        private readonly IConnectionMultiplexer multiplexer;

        public RedisController(IConnectionMultiplexer multiplexer)
        {
            this.multiplexer = multiplexer;
        }

        [HttpGet("{key}")]
        public async Task<IActionResult> GetCacheByKey(string key)
        {
            var responseContent = await multiplexer.GetDatabase().StringGetAsync(key);

            return Content(
               responseContent,
               "application/json"
           );
        }

        [HttpPost("{key}")]
        public async Task<IActionResult> PostCacheByKey(Preference data)
        {
            var requestContent = JsonConvert.SerializeObject(data);
            await multiplexer.GetDatabase().StringSetAsync(data.Id.ToString(), requestContent);
            return Ok(data.Id.ToString());
        }

        [HttpDelete("{key}")]
        public async Task<IActionResult> DeleteCacheByKey(string key)
        {
            await multiplexer.GetDatabase().KeyDeleteAsync(key);
            return Ok(key);
        }

        [HttpGet("GetCachedAll")]
        public IActionResult GetCachedAll()
        {
            var keys = multiplexer
                .GetServer(multiplexer
                    .GetEndPoints()
                    .First())
                .Keys()
                .Select(x => x.ToString());

            var preferences = new List<Preference>();
            foreach(var redisKey in keys)
            {
                preferences.Add(JsonConvert.DeserializeObject<Preference>(multiplexer.GetDatabase().StringGet(redisKey)));
            }

            return Ok(preferences);
        }
    }
}
