﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Caching.WebHost.Contracts
{
    public class Preference
    : BaseEntity
    {
        public string Name { get; set; }
    }
}
