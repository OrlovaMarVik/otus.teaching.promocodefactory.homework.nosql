﻿using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class PreferenceGateway : IPreferenceGateway
    {
        private readonly HttpClient _httpClient;

        public PreferenceGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task AddPreferenceAsync(Preference preference)
        {
            string json = JsonConvert.SerializeObject(preference);

            StringContent httpContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync($"api/v1/Redis/{preference.Id}", httpContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task<string> GetPreferenceAsync(Guid id)
        {
            var response = await _httpClient.GetAsync($"api/v1/Redis/{id}");

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> GetAllPreferenceAsync()
        {
            var response = await _httpClient.GetAsync($"api/v1/Redis/GetCachedAll");

            return await response.Content.ReadAsStringAsync();
        }
    }
}
